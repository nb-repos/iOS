//
//  String+Extension.swift
//  20230216-NikunjBhagat-NYCSchools
//
//  Created by Nikunj Bhagat on 2/16/23.
//

import Foundation

extension String {
    func localized(withComment comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
}
